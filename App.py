from copy import deepcopy
from os import makedirs
from os import path
from typing import List

log: 'Log'
settings: 'Settings'


def callback_decode(ag: 'Agent', _user_data, msg: 'Message'):
    try:
        metadata_len: int = int(msg.payload[0])
        metadata: List[str] = msg.payload[1:1 + metadata_len].decode().split("\x10")

        log.write(f"CAMERA CLIENT RECEIVED:{metadata}")
        ag.publish("console", f"CAMERA CLIENT RECEIVED:{metadata}")

        file_bin: bytes = msg.payload[1 + metadata_len:]
        file_path = path.join(
            settings["folders"]["data"],
            "camera_client",
            metadata[0],
            f"{metadata[2]}.{metadata[1]}"
        )

        makedirs(path.dirname(file_path), exist_ok=True)

        with open(file_path, "wb") as f:
            f.write(file_bin)

        log.write(f"CAMERA CLIENT SAVED:{file_path}")

    except (BaseException, Exception) as err:
        log.write(f"CAMERA CLIENT ERROR:{repr(err)}")
        ag.publish("console", f"CAMERA CLIENT ERROR:{repr(err)}")


def subscribe_topics(agent: 'Agent', topics: List[str]):
    for topic in topics:
        agent.subscribe_raw(topic)
        agent.message_callback_add_raw(topic, callback_decode)
        log.write(f"CAMERA CLIENT TOPIC ADD:{topic}")


def unsubscribe_topics(agent: 'Agent', topics: List[str]):
    for topic in topics:
        agent.unsubscribe_raw(topic)
        agent.message_callback_remove(topic)
        log.write(f"CAMERA CLIENT TOPIC DEL:{topic}")


def app(settings_arg: 'Settings', log_arg: 'Log', agent: 'Agent'):
    global settings
    global log

    settings = settings_arg
    log = log_arg

    makedirs(path.join(settings["folders"]["data"], "camera_client"), exist_ok=True)
    topics: List[str] = deepcopy(settings["camera_client"]["topics"])

    subscribe_topics(agent, topics)

    while True:
        agent.loop(1)

        topics_new: List[str] = deepcopy(settings["camera_client"]["topics"])

        if topics_new != topics:
            unsubscribe_topics(agent, [t for t in topics if t not in topics_new])
            subscribe_topics(agent, topics_new)
            topics = topics_new
